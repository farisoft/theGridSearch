/**
 * Created by farath.shba on 4/12/2017.
 */
import java.util.Scanner;
public class executor
{
    public static int[][] getCube(int row, int column)
    {
        int[][] cube = new int[row][column];
        for(int k = 0; k < column; k++)
        {
            String tmp = new Scanner(System.in).nextLine();
            //  1 2 3 4 5 6
            //  0123456789
            for(int i = 0; i < row; i++)
            {
                if(!Character.isSpaceChar(tmp.charAt(i)))
                {
                    cube[i][k] = Integer.parseInt(String.valueOf(tmp.charAt(i)));
                }

//                cube[i][k] = new Scanner(System.in).nextInt();
            }
            System.out.println();
        }

        return cube;
    };

    public static void main(String[] args)
    {
        int tries = Integer.parseInt(new Scanner(System.in).nextLine());
        int row;
        int column;

        for(int s = 0; s < tries; s++)
        {
            String nextTmpLine = new Scanner(System.in).nextLine();
            row = Integer.parseInt(String.valueOf(nextTmpLine.charAt(0)));
            column = Integer.parseInt(String.valueOf(nextTmpLine.charAt(2)));
            getCube(row, column);
        }
    }
}
